import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
    extra["joobyVersion"] = "1.6.+"

    repositories {
        mavenLocal()
        jcenter()
        mavenCentral()
    }

    dependencies {
        classpath("org.jooby:jooby-gradle-plugin:${extra["joobyVersion"]}")
    }
}

apply {
    plugin("jooby")

}

plugins {
    idea
    application
    kotlin("jvm") version "1.3.21"
    id("com.google.osdetector") version "1.6.2"
    id("io.spring.dependency-management") version "1.0.7.RELEASE"
    id("com.github.johnrengelman.shadow") version "5.0.0"
}

group = "com.cluttered.code"
version = "0.0.0"

val joobyVersion:String by extra
val postgresqlVersion: String by extra("42.+")
val jwtVersion: String by extra("3.+")

repositories {
    mavenLocal()
    jcenter()
    mavenCentral()
}

dependencyManagement {
    imports {
        mavenBom("org.jooby:jooby-bom:$joobyVersion")
    }
}


dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))


    implementation("org.jooby:jooby-lang-kotlin")
    implementation("org.jooby:jooby-gson")

    implementation("org.jooby:jooby-netty")
    implementation("io.netty:netty-transport-native-epoll:${dependencyManagement.importedProperties["netty.version"]}:${if (osdetector.classifier.contains("linux")) "linux-x86_64" else ""}")
    implementation("io.netty:netty-tcnative-boringssl-static:${dependencyManagement.importedProperties["boringssl.version"]}:${osdetector.classifier}")

    implementation("org.jooby:jooby-jdbi3")
    implementation("org.jdbi:jdbi3-postgres:${dependencyManagement.importedProperties["jdbi3.version"]}")
    implementation("org.jdbi:jdbi3-kotlin-sqlobject:${dependencyManagement.importedProperties["jdbi3.version"]}")
    implementation("org.postgresql:postgresql:$postgresqlVersion")

    implementation("com.auth0:java-jwt:${jwtVersion}")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

application {
    mainClassName = "com.cluttered.code.padlock.App"
}

/** We diverge from the default resources structure to adopt the Jooby standard: */
sourceSets {
    main {
        resources.srcDirs("conf", "public")
    }
}

tasks.withType<ShadowJar> {
    classifier = ""
    version = ""
}

