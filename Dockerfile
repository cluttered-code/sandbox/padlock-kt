FROM gradle:5.3-jdk-alpine as base

ADD --chown=gradle . /usr/src/app

WORKDIR /usr/src/app
COPY . /user/src/app/

#RUN apk update &&\
#    apk upgrade --no-cache


FROM base as build

RUN gradle shadowJar


FROM gradle:5.3-jdk-alpine as production

WORKDIR /usr/src/app
COPY --from=build /usr/src/app/build/libs/padlock-kt.jar /usr/src/app/padlock-kt.jar

#RUN apk update &&\
#    apk upgrade --no-cache

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "padlock-kt.jar"]


#FROM base
#
#EXPOSE 8080
#
#CMD ["gradle", "--no-daemon", "JoobyRun"]