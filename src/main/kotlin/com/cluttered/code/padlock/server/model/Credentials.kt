package com.cluttered.code.padlock.server.model

data class Credentials(val email: String, val password: String)