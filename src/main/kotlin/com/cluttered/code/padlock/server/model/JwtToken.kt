package com.cluttered.code.padlock.server.model

data class JwtToken(val token: String, val type: String = "Bearer")