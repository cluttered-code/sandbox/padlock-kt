package com.cluttered.code.padlock.server.model

import java.util.*

data class User(
    val uuid: UUID,
    val email: String
)