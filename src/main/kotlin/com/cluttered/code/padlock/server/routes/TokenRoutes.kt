package com.cluttered.code.padlock.server.routes

import com.cluttered.code.padlock.db.repositories.UserRepository
import com.cluttered.code.padlock.server.model.*
import com.cluttered.code.padlock.utils.signJwt
import com.cluttered.code.padlock.utils.verifyPassword
import org.jooby.*

class TokenRoutes : Kooby({
    path("tokens") {
        post("jwt") {
            val credentials = body().to<Credentials>()

            val userRepository = require(UserRepository::class)
            val user = userRepository.findByEmail(credentials.email)

            verifyPassword(credentials.password, user.salt, user.hash)
            val token = signJwt(user)
            JwtToken(token)
        }
    }
})