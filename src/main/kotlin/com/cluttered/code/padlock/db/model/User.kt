package com.cluttered.code.padlock.db.model

import org.jdbi.v3.core.mapper.RowMapper
import org.jdbi.v3.core.statement.StatementContext
import java.sql.ResultSet
import java.util.*

data class DbUser(
    val uuid: UUID,
    val email: String,
    val salt: ByteArray,
    val hash: ByteArray
) {
    class Mapper : RowMapper<DbUser> {
        override fun map(rs: ResultSet, ctx: StatementContext?): DbUser {
            return DbUser(
                rs.getObject("uuid") as UUID,
                rs.getString("email"),
                rs.getBytes("salt"),
                rs.getBytes("hash")
            )
        }
    }
}
