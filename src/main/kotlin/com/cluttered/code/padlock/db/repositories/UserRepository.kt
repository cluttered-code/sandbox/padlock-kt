package com.cluttered.code.padlock.db.repositories

import com.cluttered.code.padlock.db.model.DbUser
import org.jdbi.v3.sqlobject.statement.SqlQuery

interface UserRepository {

    @SqlQuery("SELECT * FROM users WHERE email=:email LIMIT 1")
    fun findByEmail(email: String): DbUser
}