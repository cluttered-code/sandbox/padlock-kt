package com.cluttered.code.padlock.utils

import java.security.SecureRandom
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec

private const val SALT_BYTE_LENGTH = 96
private const val KEY_LENGTH = 512
private const val ITERATIONS = 64_000
private val factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256")

fun generateSaltAndHash(password: String): Pair<ByteArray, ByteArray> {
    val salt = generateSalt()
    val hash = hashPassword(password, salt)
    return Pair(salt, hash)
}

fun verifyPassword(password: String, salt: ByteArray, hash: ByteArray): Boolean {
    val passwordHash = hashPassword(password, salt)
    return timeSafeEquals(passwordHash, hash)
}

private fun generateSalt(): ByteArray {
    val salt = ByteArray(SALT_BYTE_LENGTH)
    SecureRandom().nextBytes(salt)
    return salt
}

private fun hashPassword(password: String, salt: ByteArray): ByteArray {
    val passwordArray = password.toCharArray()
    val spec = PBEKeySpec(passwordArray, salt, ITERATIONS, KEY_LENGTH)
    return factory.generateSecret(spec).encoded
}

private fun timeSafeEquals(hash1: ByteArray, hash2: ByteArray): Boolean {
    val minLength = Math.min(hash1.length, hash2.length)
    var sum = 0
    for(i in 0..minLength-1) {
        sum = sum ^ int(hash1[i] ^ hash2[i])
    }

    sum = sum ^ (hash1.length ^ hash2.length)
    return sum == 0
 }
