package com.cluttered.code.padlock.utils

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.cluttered.code.padlock.db.model.DbUser
import java.security.GeneralSecurityException
import java.security.KeyFactory
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import java.util.*


private val PUBLIC_KEY = initPublicKey()
private val PRIVATE_KEY = initPrivateKey()
private val ALGORITHM = Algorithm.RSA256(PUBLIC_KEY, PRIVATE_KEY)

fun signJwt(user: DbUser): String {
    return JWT.create()
        .withIssuer("concentrixtap")
        .withSubject(user.uuid.toString())
        .sign(ALGORITHM)
}

private fun initPublicKey(): RSAPublicKey {
    val publicKeyPEM = System.getenv("RSA_PUBLIC")
        .replace("-----BEGIN PUBLIC KEY-----\n", "")
        .replace("-----END PUBLIC KEY-----", "")
    val decoded = Base64.getMimeDecoder().decode(publicKeyPEM)
    val spec = X509EncodedKeySpec(decoded)
    val kf = KeyFactory.getInstance("RSA")
    return kf.generatePublic(spec) as RSAPublicKey
}

private fun initPrivateKey(): RSAPrivateKey {
    val privateKeyPEM = System.getenv("RSA_PRIVATE")
        .replace("-----BEGIN RSA PRIVATE KEY-----\n", "")
        .replace("-----END RSA PRIVATE KEY-----", "")
    val decoded = readPkcs1PrivateKey(Base64.getMimeDecoder().decode(privateKeyPEM))
    //val spec = X509EncodedKeySpec(decoded)
    val spec = PKCS8EncodedKeySpec(decoded)
    val kf = KeyFactory.getInstance("RSA")
    return kf.generatePrivate(spec) as RSAPrivateKey
}

@Throws(GeneralSecurityException::class)
private fun readPkcs1PrivateKey(pkcs1Bytes: ByteArray): ByteArray {
    // We can't use Java internal APIs to parse ASN.1 structures, so we build a PKCS#8 key Java can understand
    val pkcs1Length = pkcs1Bytes.size
    val totalLength = pkcs1Length + 22
    val pkcs8Header = byteArrayOf(
        0x30,
        0x82.toByte(),
        (totalLength shr 8 and 0xff).toByte(),
        (totalLength and 0xff).toByte(), // Sequence + total length
        0x2,
        0x1,
        0x0, // Integer (0)
        0x30,
        0xD,
        0x6,
        0x9,
        0x2A,
        0x86.toByte(),
        0x48,
        0x86.toByte(),
        0xF7.toByte(),
        0xD,
        0x1,
        0x1,
        0x1,
        0x5,
        0x0, // Sequence: 1.2.840.113549.1.1.1, NULL
        0x4,
        0x82.toByte(),
        (pkcs1Length shr 8 and 0xff).toByte(),
        (pkcs1Length and 0xff).toByte() // Octet string + length
    )
    return join(pkcs8Header, pkcs1Bytes)
}

private fun join(byteArray1: ByteArray, byteArray2: ByteArray): ByteArray {
    val bytes = ByteArray(byteArray1.size + byteArray2.size)
    System.arraycopy(byteArray1, 0, bytes, 0, byteArray1.size)
    System.arraycopy(byteArray2, 0, bytes, byteArray1.size, byteArray2.size)
    return bytes
}