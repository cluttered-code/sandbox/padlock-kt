package com.cluttered.code.padlock

import com.cluttered.code.padlock.db.repositories.UserRepository
import com.cluttered.code.padlock.server.routes.TokenRoutes
import org.jdbi.v3.core.kotlin.KotlinPlugin
import org.jdbi.v3.postgres.PostgresPlugin
import org.jdbi.v3.sqlobject.SqlObjectPlugin
import org.jdbi.v3.sqlobject.kotlin.KotlinSqlObjectPlugin
import org.jooby.Jooby.*
import org.jooby.Kooby
import org.jooby.jdbc.Jdbc
import org.jooby.jdbi.Jdbi3
import org.jooby.jdbi.TransactionalRequest
import org.jooby.json.Gzon

/**
 * Gradle Kotlin stater project.
 */
class App : Kooby({

    use(Gzon())

    use(Jdbc())
    use(Jdbi3().doWith { jdbi, _ ->
        jdbi.installPlugin(SqlObjectPlugin())
        jdbi.installPlugin(KotlinPlugin())
        jdbi.installPlugin(KotlinSqlObjectPlugin())
        jdbi.installPlugin(PostgresPlugin())
    }.transactionPerRequest(TransactionalRequest().attach(UserRepository::class.java)))

    path("v0") {
        use(TokenRoutes())
    }

    get("ping") {
        "pong"
    }
}) {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            run(::App, args)
        }
    }
}